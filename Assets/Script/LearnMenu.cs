﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LearnMenu : MonoBehaviour
{
    private bool isCanStart = false;
    IEnumerator Wait()
    {
        yield return new WaitForSecondsRealtime(3);
        isCanStart = true;
    }

    private void Awake()
    {
        StartCoroutine(Wait());
        Cursor.visible = false;
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && isCanStart == true)
        {
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Cursor.visible = true;
            
        }
    }

}
