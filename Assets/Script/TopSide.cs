﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopSide : MonoBehaviour
{
    GameObject[] TopS;
    GameObject[] DivS;
    GameObject[] DivSOff;
    public float CountDown = 1.0f;
    public float Timer;
    public Material Green;
    public Material Red;
    public GameObject LoseCanvas;
    
    // Start is called before the first frame update
    void Start()
    {
 

    }

    // Update is called once per frame
    void Update()
    {   
        
        TopS = GameObject.FindGameObjectsWithTag("Dis");
        Timer += Time.deltaTime;
        CountDown -= Time.deltaTime;
        if(TopS.Length > 20)
        {
                if(Timer > 0 & Timer < 10)
                {

                    if (CountDown <= 0.0f)
                    {
                        TopS = GameObject.FindGameObjectsWithTag("Dis");
                        GameObject rand = TopS[Random.Range(0, TopS.Length)]; 
                        rand.GetComponent<MeshRenderer>().enabled = true;
                        rand.GetComponent<Collider>().enabled = true;
                        rand.tag = "Side";
                        CountDown = 0.8f;
            
           
                    }
                }
                if(Timer >= 10 & Timer < 20)
                {
                    if (CountDown <= 0.0f)
                    {
                        TopS = GameObject.FindGameObjectsWithTag("Dis");
                        GameObject rand = TopS[Random.Range(0, TopS.Length)];
                        rand.GetComponent<MeshRenderer>().enabled = true;
                        rand.GetComponent<Collider>().enabled = true;
                        rand.tag = "Side";
                        CountDown = 0.6f;


                    }
                }
                if (Timer >= 20 & Timer < 40)
                {
                    DivS = GameObject.FindGameObjectsWithTag("SideDiv");
                    foreach (GameObject DivSide in DivS)
                        DivSide.GetComponent<MeshRenderer>().enabled = true;
                    foreach (GameObject DivSide in DivS)
                        DivSide.GetComponent<Collider>().enabled = true;


                    if (CountDown <= 0.0f)
                    {
                        TopS = GameObject.FindGameObjectsWithTag("Dis");
                        GameObject rand = TopS[Random.Range(0, TopS.Length)];
                        rand.GetComponent<MeshRenderer>().enabled = true;
                        rand.GetComponent<Collider>().enabled = true;
                        rand.tag = "Side";
                        CountDown = 0.4f;


                    }
                }
                if (Timer >= 40 & Timer < 50)
                {
                    DivS = GameObject.FindGameObjectsWithTag("SideDiv");
                    foreach (GameObject DivSide in DivS)
                        DivSide.tag = "SideDivOff";
                    foreach (GameObject DivSide in DivS)
                        DivSide.GetComponent<Renderer>().material = Green;
            
            

                     if (CountDown <= 0.0f)
                     {
                
                        TopS = GameObject.FindGameObjectsWithTag("Dis");
                        GameObject rand = TopS[Random.Range(0, TopS.Length)];
                        rand.GetComponent<MeshRenderer>().enabled = true;
                        rand.GetComponent<Collider>().enabled = true;
                        rand.tag = "Side";

                        CountDown = 0.2f;

                     }
            
                }
                if(Timer >= 50)
                {
                    DivSOff = GameObject.FindGameObjectsWithTag("SideDivOff");
                    foreach (GameObject DivSideOff in DivSOff)
                        DivSideOff.tag = "SideDiv";
                    foreach (GameObject DivSideOff in DivSOff)
                        DivSideOff.GetComponent<Renderer>().material = Red;

                    Timer = 0 * Timer + 20;
                }
            
        }
        else
        {
            LoseCanvas.SetActive(true);
            Time.timeScale = 0;
        }   









    }
}
