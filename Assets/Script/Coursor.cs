﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coursor : MonoBehaviour
{
    public Texture2D CoursorTexture;
    public CursorMode CursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

     void Start()
    {
        Cursor.SetCursor(CoursorTexture, hotSpot, CursorMode);
    }

}
