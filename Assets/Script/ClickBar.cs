﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickBar : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider Slider;
    public DestoyCubes DestoyCubes;

    public void Update()
    {
        Slider.value = DestoyCubes.ShootCount;
    }
}
