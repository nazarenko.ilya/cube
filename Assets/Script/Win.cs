﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    GameObject[] WinSphere;
    public GameObject WinText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        WinSphere = GameObject.FindGameObjectsWithTag("FinalSP");
        if (WinSphere.Length > 0)
        {
            WinText.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
