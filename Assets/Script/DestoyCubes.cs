﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyCubes : MonoBehaviour
{
    public float ShootCount = 50;
    Animator m_Animator;
    public bool isSphereClick;


    [SerializeField]
    private AudioClip POPsound;
    [SerializeField]
    private AudioClip SpherePOP;
    AudioSource AudioSource;

    


    void Start()
    {
        m_Animator = GetComponent<Animator>();
        AudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        m_Animator.SetBool("isSphereClick", isSphereClick);

        if (Time.timeScale > 0)
        {
            if (Input.GetMouseButtonDown(0))
            {

                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
                    RaycastHit _hit;
                    if (Physics.Raycast(ray, out _hit, Mathf.Infinity))
                    {
                
                            if (_hit.transform.gameObject.CompareTag("Sphere"))
                            {
                                isSphereClick = true;
                                ShootCount -= 1;
                                AudioSource.PlayOneShot(SpherePOP);
                                if (ShootCount <= 0 & ShootCount > -1)
                                {
                                    _hit.transform.gameObject.GetComponent<MeshRenderer>().enabled = false;
                                    _hit.transform.gameObject.GetComponent<Collider>().enabled = false;
                                    _hit.transform.gameObject.tag = "FinalSP";

                                }
                            }
                            else if (_hit.transform.gameObject.CompareTag("SideDiv"))
                            {

                            }
                            else if (_hit.transform.gameObject.CompareTag("SideDivOff"))
                            {
                                _hit.transform.gameObject.GetComponent<MeshRenderer>().enabled = false;
                                _hit.transform.gameObject.GetComponent<Collider>().enabled = false;
                                _hit.transform.gameObject.tag = "SideDivOff";
                                AudioSource.PlayOneShot(POPsound);
                    }
                            else
                            {

                                _hit.transform.gameObject.tag = "Dis";
                                _hit.transform.gameObject.GetComponent<MeshRenderer>().enabled = false;
                                _hit.transform.gameObject.GetComponent<Collider>().enabled = false;
                                AudioSource.PlayOneShot(POPsound);

                            }

                    }
            
            
            }
            if (Input.GetMouseButtonUp(0))
            {
                isSphereClick = false;
            }
        }
            
    }
}
