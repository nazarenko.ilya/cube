﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DivCubes : MonoBehaviour
{
    public TopSide TopSide;
    public float CountDown3 = 0.2f;
    GameObject[] DivSOff;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if(TopSide.Timer >= 40 & TopSide.Timer < 50)
        {
            CountDown3 -= Time.deltaTime;
            if (CountDown3 <= 0)
            {
                DivSOff = GameObject.FindGameObjectsWithTag("SideDivOff");
                GameObject RandDiv = DivSOff[Random.Range(0, DivSOff.Length)];
                RandDiv.GetComponent<MeshRenderer>().enabled = true;
                RandDiv.GetComponent<Collider>().enabled = true;

                CountDown3 = 0.2f;
            }
        }
    }
}
